<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "usos".
 *
 * @property int $id
 * @property int|null $codigo
 * @property string|null $usos
 *
 * @property Epis $codigo0
 */
class Usos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'usos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo'], 'integer'],
            [['usos'], 'string', 'max' => 200],
            [['codigo', 'usos'], 'unique', 'targetAttribute' => ['codigo', 'usos']],
            [['codigo'], 'exist', 'skipOnError' => true, 'targetClass' => Epis::className(), 'targetAttribute' => ['codigo' => 'codigo']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'codigo' => 'Codigo',
            'usos' => 'Usos',
        ];
    }

    /**
     * Gets query for [[Codigo0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigo0()
    {
        return $this->hasOne(Epis::className(), ['codigo' => 'codigo']);
    }
}
