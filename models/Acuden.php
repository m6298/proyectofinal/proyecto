<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "acuden".
 *
 * @property int $id
 * @property int|null $codigo
 * @property int|null $idcuadrilla
 *
 * @property Alertas $codigo0
 * @property Cuadrillas $idcuadrilla0
 */
class Acuden extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'acuden';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo', 'idcuadrilla'], 'integer'],
            [['codigo', 'idcuadrilla'], 'unique', 'targetAttribute' => ['codigo', 'idcuadrilla']],
            [['codigo'], 'exist', 'skipOnError' => true, 'targetClass' => Alertas::className(), 'targetAttribute' => ['codigo' => 'codigo']],
            [['idcuadrilla'], 'exist', 'skipOnError' => true, 'targetClass' => Cuadrillas::className(), 'targetAttribute' => ['idcuadrilla' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'codigo' => 'Codigo',
            'idcuadrilla' => 'Idcuadrilla',
        ];
    }

    /**
     * Gets query for [[Codigo0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigo0()
    {
        return $this->hasOne(Alertas::className(), ['codigo' => 'codigo']);
    }

    /**
     * Gets query for [[Idcuadrilla0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdcuadrilla0()
    {
        return $this->hasOne(Cuadrillas::className(), ['id' => 'idcuadrilla']);
    }

    public function getdropdownCuadrillas(){


        $models = Cuadrillas::find()->asArray()->select(' num_cuadrilla')->all();
        return ArrayHelper::map($models, 'num_cuadrilla', 'num_cuadrilla');

    }
}
