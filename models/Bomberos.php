<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
/**
 * This is the model class for table "bomberos".
 *
 * @property int $id
 * @property string|null $codigo
 * @property int|null $idcuadrilla
 * @property string|null $rango
 * @property string|null $nombre
 * @property string|null $apellidos
 * @property int|null $disponible
 *
 * @property Epis $epis
 * @property FuncionesBombero[] $funcionesBomberos
 * @property Cuadrillas $idcuadrilla0
 * @property Vehiculos[] $matriculas
 * @property Utilizan[] $utilizans
 */
class Bomberos extends \yii\db\ActiveRecord
{

    public $zona;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'bomberos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idcuadrilla', 'disponible'], 'integer'],
            [['codigo', 'rango', 'nombre'], 'string', 'max' => 200],
            [['apellidos'], 'string', 'max' => 400],
            [['apellidos'],"required"],
            [['codigo'],'unique','message'=>"Ya existe"],
            [['codigo','nombre','idcuadrilla','rango'],'required'],
            [['codigo'],'integer'],
            [['idcuadrilla'], 'exist', 'skipOnError' => true, 'targetClass' => Cuadrillas::className(), 'targetAttribute' => ['idcuadrilla' => 'id']],
            [['nombre','apellidos'],'match', 'pattern' => '/^[a-zA-ZÀ-ÿ\s]{0,60}$/','message' =>'No se permiten numero en este campo'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'codigo' => 'Codigo',
            'idcuadrilla' => 'Idcuadrilla',
            'rango' => 'Rango',
            'nombre' => 'Nombre',
            'apellidos' => 'Apellidos',
            'disponible' => 'Disponible',
        ];
    }

    public function yes_or_no($val){

     return $val == 1 ? 'si' : 'no';

    }
    /**
     * Gets query for [[Epis]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEpis()
    {
        return $this->hasOne(Epis::className(), ['codigo_bombero' => 'codigo']);
    }

    /**
     * Gets query for [[FuncionesBomberos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getFuncionesBomberos()
    {
        return $this->hasMany(FuncionesBombero::className(), ['codigo' => 'codigo']);
    }

    /**
     * Gets query for [[Idcuadrilla0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdcuadrilla0()
    {
        return $this->hasOne(Cuadrillas::className(), ['id' => 'idcuadrilla']);
    }

    /**
     * Gets query for [[Matriculas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMatriculas()
    {
        return $this->hasMany(Vehiculos::className(), ['matricula' => 'matricula'])->viaTable('utilizan', ['codigo_bombero' => 'codigo']);
    }

    /**
     * Gets query for [[Utilizans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigo()
    {
        return $this->hasMany(Utilizan::className(), ['codigo_bombero' => 'codigo']);
    }
    
    public function getNombre(){
        return "codigo";
    }
    public function getdropdownIdCuadrillas(){

        /*
         * Devuelve todos las cuadrillas en un menu desplegable utilizado en los formularios
         */

        $models = Cuadrillas::find()->asArray()->select('id, num_cuadrilla')->orderBy('num_cuadrilla')->all();
        return ArrayHelper::map($models, 'id', 'num_cuadrilla');

    }
}
