<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "funciones_vehiculo".
 *
 * @property int $id
 * @property string|null $matricula
 * @property string|null $funciones
 *
 * @property Vehiculos $matricula0
 */
class FuncionesVehiculo extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'funciones_vehiculo';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['matricula'], 'string', 'max' => 8],
            [['funciones'], 'string', 'max' => 200],
            [['matricula', 'funciones'], 'unique', 'targetAttribute' => ['matricula', 'funciones']],
            [['matricula'], 'exist', 'skipOnError' => true, 'targetClass' => Vehiculos::className(), 'targetAttribute' => ['matricula' => 'matricula']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'matricula' => 'Matricula',
            'funciones' => 'Funciones',
        ];
    }

    /**
     * Gets query for [[Matricula0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMatricula0()
    {
        return $this->hasOne(Vehiculos::className(), ['matricula' => 'matricula']);
    }
}
