<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "funciones_bombero".
 *
 * @property int $id
 * @property string|null $codigo
 * @property string|null $funciones
 *
 * @property Bomberos $codigo0
 */
class FuncionesBombero extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'funciones_bombero';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo'], 'string', 'max' => 200],
            [['funciones'], 'string', 'max' => 300],
            [['codigo', 'funciones'], 'unique', 'targetAttribute' => ['codigo', 'funciones']],
            [['codigo'], 'exist', 'skipOnError' => true, 'targetClass' => Bomberos::className(), 'targetAttribute' => ['codigo' => 'codigo']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'codigo' => 'Codigo',
            'funciones' => 'Funciones',
        ];
    }

    /**
     * Gets query for [[Codigo0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigo0()
    {
        return $this->hasOne(Bomberos::className(), ['codigo' => 'codigo']);
    }
}
