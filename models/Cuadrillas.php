<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "cuadrillas".
 *
 * @property int $id
 * @property int|null $num_cuadrilla
 * @property string|null $zona
 * @property int|null $disponible
 * @property int|null $num_bomberos
 *
 * @property Acuden[] $acudens
 * @property Bomberos[] $bomberos
 * @property Alertas[] $codigos
 * @property Parques $zona0
 */
class Cuadrillas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cuadrillas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['num_cuadrilla', 'disponible', 'num_bomberos'], 'integer'],
            [['zona'], 'string', 'max' => 200],
            [['num_cuadrilla', 'zona'], 'unique', 'targetAttribute' => ['num_cuadrilla', 'zona']],
            [['zona'], 'exist', 'skipOnError' => true, 'targetClass' => Parques::className(), 'targetAttribute' => ['zona' => 'zona']],
            [['num_cuadrilla','num_bomberos'], 'required']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'num_cuadrilla' => 'Num Cuadrilla',
            'zona' => 'Zona',
            'disponible' => 'Disponible',
            'num_bomberos' => 'Num Bomberos',
        ];
    }

    /**
     * Gets query for [[Acudens]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAcudens()
    {
        return $this->hasMany(Acuden::className(), ['idcuadrilla' => 'id']);
    }

    /**
     * Gets query for [[Bomberos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getBomberos()
    {
        return $this->hasMany(Bomberos::className(), ['idcuadrilla' => 'id']);
    }

    /**
     * Gets query for [[Codigos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigos()
    {
        return $this->hasMany(Alertas::className(), ['codigo' => 'codigo'])->viaTable('acuden', ['idcuadrilla' => 'id']);
    }

    /**
     * Gets query for [[Zona0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getZona0()
    {
        return $this->hasOne(Parques::className(), ['zona' => 'zona']);
    }
}
