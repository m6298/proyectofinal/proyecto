<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
/**
 * This is the model class for table "vehiculos".
 *
 * @property int $id
 * @property string|null $matricula
 * @property string|null $tipo
 * @property string|null $zona
 *
 * @property Bomberos[] $codigoBomberos
 * @property FuncionesVehiculo[] $funcionesVehiculos
 * @property Utilizan[] $utilizans
 * @property Parques $zona0
 */
class Vehiculos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'vehiculos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['matricula'], 'string', 'max' => 8],
            [['tipo'], 'string', 'max' => 100],
            //[['zona'], 'string', 'max' => 200],
            [['matricula'], 'unique'],
            [['matricula','tipo','zona'], 'required'],
            [['matricula'], 'match', 'pattern' => '/^[0-9]{4}[-]{1}[A-Z]{3}/i'],
            [['zona'], 'exist', 'skipOnError' => true, 'targetClass' => Parques::className(), 'targetAttribute' => ['zona' => 'zona']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'matricula' => 'Matricula',
            'tipo' => 'Tipo',
            'zona' => 'Zona',
        ];
    }

    /**
     * Gets query for [[CodigoBomberos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoBomberos()
    {
        return $this->hasMany(Bomberos::className(), ['codigo' => 'codigo_bombero'])->viaTable('utilizan', ['matricula' => 'matricula']);
    }

    /**
     * Gets query for [[FuncionesVehiculos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getFuncionesVehiculos()
    {
        return $this->hasMany(FuncionesVehiculo::className(), ['matricula' => 'matricula']);
    }

    /**
     * Gets query for [[Utilizans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUtilizans()
    {
        return $this->hasMany(Utilizan::className(), ['matricula' => 'matricula']);
    }

    /**
     * Gets query for [[Zona0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getZona0()
    {
        return $this->hasOne(Parques::className(), ['zona' => 'zona']);
    }

    public function getdropdownZonas(){


        $models = Parques::find()->asArray()->select(' zona')->all();
        return ArrayHelper::map($models, 'zona', 'zona');

    }

}
