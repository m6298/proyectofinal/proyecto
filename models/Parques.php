<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "parques".
 *
 * @property int $id
 * @property string|null $zona
 * @property float|null $radio_accion
 *
 * @property Cuadrillas[] $cuadrillas
 * @property Vehiculos[] $vehiculos
 */
class Parques extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'parques';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['radio_accion'], 'number'],
            [['zona'], 'string', 'max' => 200],
            [['zona'], 'unique'],
            [['radio_accion','zona'],'required'],
            [['zona'],'match', 'pattern' => '/^[a-zA-ZÀ-ÿ\s]{0,60}$/','message' =>'No se permiten numero en este campo'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'zona' => 'Zona',
            'radio_accion' => 'Radio Accion',
        ];
    }

    /**
     * Gets query for [[Cuadrillas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCuadrillas()
    {
        return $this->hasMany(Cuadrillas::className(), ['zona' => 'zona']);
    }

    /**
     * Gets query for [[Vehiculos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getVehiculos()
    {
        return $this->hasMany(Vehiculos::className(), ['zona' => 'zona']);
    }
}
