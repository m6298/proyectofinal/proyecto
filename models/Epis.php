<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "epis".
 *
 * @property int $codigo
 * @property int|null $nivel_proteccion
 * @property string|null $codigo_bombero
 *
 * @property Bomberos $codigoBombero
 * @property Usos[] $usos
 */
class Epis extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'epis';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nivel_proteccion'], 'integer'],
            [['codigo_bombero'], 'string', 'max' => 200],
            [['codigo_bombero'], 'unique'],
            [['codigo_bombero'], 'exist', 'skipOnError' => true, 'targetClass' => Bomberos::className(), 'targetAttribute' => ['codigo_bombero' => 'codigo']],
            [['codigo_bombero','nivel_proteccion'],"required"],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo' => 'Codigo',
            'nivel_proteccion' => 'Nivel Proteccion',
            'codigo_bombero' => 'Codigo Bombero',
        ];
    }

    /**
     * Gets query for [[CodigoBombero]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoBombero()
    {
        return $this->hasOne(Bomberos::className(), ['codigo' => 'codigo_bombero']);
    }

    /**
     * Gets query for [[Usos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUsos()
    {
        return $this->hasMany(Usos::className(), ['codigo' => 'codigo']);
    }
    public function getdropdownNivelProt(){

        

        $models = Epis::find()->asArray()->select('nivel_proteccion')->groupBy('nivel_proteccion')->orderBy('nivel_proteccion')->all();
        return ArrayHelper::map($models, 'nivel_proteccion', 'nivel_proteccion');

    }
    public function getdropdownCodigoBombero(){

       

        $models = Bomberos::find()->asArray()->select('codigo')->groupBy('codigo')->orderBy('codigo')->all();
        return ArrayHelper::map($models, 'codigo', 'codigo');

    }
}
