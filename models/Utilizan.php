<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "utilizan".
 *
 * @property int $id
 * @property string|null $codigo_bombero
 * @property string|null $matricula
 *
 * @property Bomberos $codigoBombero
 * @property Vehiculos $matricula0
 */
class Utilizan extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'utilizan';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo_bombero'], 'string', 'max' => 200],
            [['matricula'], 'string', 'max' => 8],
            [['codigo_bombero', 'matricula'], 'unique', 'targetAttribute' => ['codigo_bombero', 'matricula']],
            [['codigo_bombero'], 'exist', 'skipOnError' => true, 'targetClass' => Bomberos::className(), 'targetAttribute' => ['codigo_bombero' => 'codigo']],
            [['matricula'], 'exist', 'skipOnError' => true, 'targetClass' => Vehiculos::className(), 'targetAttribute' => ['matricula' => 'matricula']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'codigo_bombero' => 'Codigo Bombero',
            'matricula' => 'Matricula',
        ];
    }

    /**
     * Gets query for [[CodigoBombero]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoBombero()
    {
        return $this->hasOne(Bomberos::className(), ['codigo' => 'codigo_bombero']);
    }

    /**
     * Gets query for [[Matricula0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMatricula0()
    {
        return $this->hasOne(Vehiculos::className(), ['matricula' => 'matricula']);
    }
}
