<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "alertas".
 *
 * @property int $codigo
 * @property int|null $epis_utilizados
 * @property int|null $vehiculos_utilizados
 * @property string|null $tipo
 * @property string|null $descripcion
 *
 * @property Acuden[] $acudens
 * @property Cuadrillas[] $idcuadrillas
 */
class Alertas extends \yii\db\ActiveRecord
{

    public $zona;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'alertas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['epis_utilizados', 'vehiculos_utilizados'], 'integer'],
            [['tipo'], 'string', 'max' => 200],
            [['descripcion'], 'string', 'max' => 500],
            [['epis_utilizados', 'vehiculos_utilizados','tipo','descripcion'],'required'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo' => 'Codigo',
            'epis_utilizados' => 'Epis Utilizados',
            'vehiculos_utilizados' => 'Vehiculos Utilizados',
            'tipo' => 'Tipo',
            'descripcion' => 'Descripcion',
        ];
    }

    /**
     * Gets query for [[Acudens]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAcudens()
    {
        return $this->hasMany(Acuden::className(), ['codigo' => 'codigo']);
    }

    /**
     * Gets query for [[Idcuadrillas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdcuadrillas()
    {
        return $this->hasMany(Cuadrillas::className(), ['id' => 'idcuadrilla'])->viaTable('acuden', ['codigo' => 'codigo']);
    }

    public function getdropdownTipos(){

        /*
         * Devuelve todos las cuadrillas en un menu desplegable utilizado en los formularios
         */

        $models = Alertas::find()->asArray()->select(' tipo')->groupBy('tipo')->orderBy('tipo')->all();
        return ArrayHelper::map($models, 'tipo', 'tipo');

    }
}
