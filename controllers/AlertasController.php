<?php

namespace app\controllers;

use app\models\Alertas;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use Yii;
use yii\web\BadRequestHttpException;
use yii\web\Response;
use yii\widgets\ActiveForm;

/**
 * AlertasController implements the CRUD actions for Alertas model.
 */
class AlertasController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Alertas models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Alertas::find(),
            /*
            'pagination' => [
                'pageSize' => 50
            ],
            'sort' => [
                'defaultOrder' => [
                    'codigo' => SORT_DESC,
                ]
            ],
            */
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Alertas model.
     * @param int $codigo Codigo
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($codigo)
    {
        return $this->render('view', [
            'model' => $this->findModel($codigo),
        ]);
    }

    /**
     * Creates a new Alertas model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new Alertas();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'codigo' => $model->codigo]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }


    public function actionCreate2()
    {
        $model = new Alertas();

        if ($model->load(Yii::$app->request->post()) ) {
            
            if( $model->save()){
            return $this->redirect(['site/alertas2']);
            }else{
                Yii::$app->session->setFlash('error', "Codigo repetido.");
                $dataProvider = new ActiveDataProvider([
                    'query' => Alertas::find()
                    ->select("alertas.codigo as ,epis_utilizados,vehiculos_utilizados, alertas.idcuadrilla as Numero Cuadrilla" )
                    ->join("LEFT JOIN","acuden","alertas.codigo=acuden.codigo")
                    ->join("INNER JOIN", "cuadrillas" ,"acuden.idcuadrilla=cuadrillas.id")
                       ,
                   'pagination' => [
                       'pageSize' => 1000,
                   ]
               ]);
                
               
                return $this ->render("..\site\datosAlertas2",[
                    "resultados"=>$dataProvider,
                    
                    "titulo"=>"Alertas",
                    
                    
                ]);
            }
        } 
        elseif (Yii::$app->request->isAjax) {
            return $this->renderAjax('create2', [
                        'model' => $model
            ]);
        } 
        else {
            
            return $this->render('create2', [
                'model' => $model, 
            ]);
        }
    }
    

    /**
     * Updates an existing Alertas model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $codigo Codigo
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($codigo)
    {
        $model = $this->findModel($codigo);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'codigo' => $model->codigo]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Alertas model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $codigo Codigo
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($codigo)
    {
        $this->findModel($codigo)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Alertas model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $codigo Codigo
     * @return Alertas the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($codigo)
    {
        if (($model = Alertas::findOne(['codigo' => $codigo])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    
}
