<?php

namespace app\controllers;

use app\models\Alertas;
use app\models\Bomberos;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Cuadrillas;
use app\models\Vehiculos;
use app\widgets\Alert;
use PHPUnit\Framework\Constraint\IsNull;
use yii\base\View;
use yii\data\SqlDataProvider;
use yii\helpers\Json;
use yii\data\ActiveDataProvider;


class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
    
     public function actionBomberos() {
        $dataProvider = new SqlDataProvider(['sql' =>'SELECT codigo as "código",idcuadrilla AS "Cuadrilla", rango, nombre, apellidos, bomberos.disponible AS "disponible",zona 
                                                    FROM bomberos 
                                                    INNER JOIN cuadrillas 
                                                    ON(idcuadrilla=cuadrillas.id)',
                                                    'pagination' => [
                                                        'pageSize' => 10,
                                                    ]
        ]);

        return $this ->render("datosBomberos",[
            "resultados"=>$dataProvider,
            "campos"=>['codigo','Cuadrilla','rango','nombre','apellidos','disponible','zona'],
            "titulo"=>"Bomberos",
            "descripcion"=>"Bomberos que hay <p>Esta disponible si el valor es 1</p>",
            
        ]);
    }
    public function actionBomberos2() {
        
        $dataProvider = new ActiveDataProvider([
            'query' => Bomberos::find()
               ->select("codigo,idcuadrilla AS Cuadrilla, rango, nombre, apellidos, bomberos.disponible AS disponible,zona" )
               ->join("INNER JOIN","Cuadrillas", 'idcuadrilla=cuadrillas.id')
               ,
           'pagination' => [
               'pageSize' => 1000,
           ]
       ]);
        

        return $this ->render("datosBomberos2",[
            "resultados"=>$dataProvider,
            "campos"=>['codigo','Cuadrilla','rango','nombre','apellidos','disponible','zona'],
            "titulo"=>"Bomberos",
            "descripcion"=>"Bomberos que hay <p>Esta disponible si el valor es 1</p>",
            
        ]);
    }
    
    public function actionVehiculos() {
        $dataProvider = new SqlDataProvider(['sql' =>'SELECT matricula ,tipo,zona FROM vehiculos ',
        ]);
        return $this ->render("datosVehiculos",[
            "resultados"=>$dataProvider,
            "campos"=>['matricula','tipo','zona'],
            "titulo"=>"Vehiculos",
           
            
        ]);
    }

    public function actionVehiculos2() {
        
        $dataProvider = new ActiveDataProvider([
            'query' => Vehiculos::find()
               ->select("matricula,tipo,zona" )
               
               ,
           'pagination' => [
               'pageSize' => 1000,
           ]
       ]);
        

        return $this ->render("datosVehiculos2",[
            "resultados"=>$dataProvider,
            "titulo"=>"Vehiculos",
            "descripcion"=>"Bomberos que hay <p>Esta disponible si el valor es 1</p>",
            
        ]);
    }
    
    public function actionFuncionalidad() {
        $dataProvider = new SqlDataProvider(['sql' =>'SELECT vehiculos.matricula AS matricula,tipo,GROUP_CONCAT(funciones) as funciones FROM vehiculos LEFT JOIN funciones_vehiculo USING (matricula) GROUP BY matricula HAVING funciones IS NOT NULL',
        ]);
        return $this ->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['matricula','tipo','funciones'],
            "titulo"=>"Vehiculos y Funcionalidades",
            "descripcion"=>"Vehiculos con funciones especificas",
            
        ]);
    }
    
    public function actionAlertas() {
        $dataProvider = new SqlDataProvider(['sql' =>'SELECT alertas.codigo as codigo,epis_utilizados,vehiculos_utilizados,tipo,acuden.idcuadrilla as "Numero Cuadrilla", zona 
                                                        FROM alertas 
                                                        LEFT JOIN acuden  USING(codigo) 
                                                        INNER JOIN cuadrillas ON(acuden.idcuadrilla=cuadrillas.id)',
        ]);

        $alertasin = new ActiveDataProvider([
            'query' => Alertas::find()
               ->select("alertas.codigo as codigo , epis_utilizados, vehiculos_utilizados,tipo" )
               ->join("LEFT JOIN","acuden","alertas.codigo=acuden.codigo")
               ->where("acuden.codigo is null")
               ,
           'pagination' => [
               'pageSize' => 1000,
           ]
        ]);


        return $this ->render("datosAlertas",[
            "resultados"=>$dataProvider,
            "sincodigo"=>$alertasin,
            "campos"=>['codigo','epis_utilizados','vehiculos_utilizados','Numero Cuadrilla','zona'],
            "titulo"=>"Alertas",


            
            
        ]);
    }

    public function actionAlertas2() {
        
        $dataProvider = new ActiveDataProvider([
            'query' => Alertas::find()
               ->select("alertas.codigo as codigo , epis_utilizados, vehiculos_utilizados,tipo, acuden.idcuadrilla as Cuadrilla, cuadrillas.zona" )
               ->join("LEFT JOIN","acuden","alertas.codigo=acuden.codigo")
               ->join("INNER JOIN", "cuadrillas" ,"acuden.idcuadrilla=cuadrillas.id")
               ,
           'pagination' => [
               'pageSize' => 1000,
           ]
        ]);


        return $this ->render("datosAlertas2",[
            "resultados"=>$dataProvider,
            "campos"=>['codigo','epis_utilizados','vehiculos_utilizados','Numero Cuadrilla','zona'],
            "titulo"=>"Alertas",
            
            
        ]);
    }
    
    public function actionParques() {
        $dataProvider = new SqlDataProvider(['sql' =>'SELECT id,zona,radio_accion FROM parques ',
        ]);
        return $this ->render("datosParques",[
            "resultados"=>$dataProvider,
            
            "titulo"=>"Parques",
           
            
        ]);
    }
    
     public function actionEpis() {
        $dataProvider = new SqlDataProvider(['sql' =>'SELECT DISTINCT e.codigo as "codigo",nivel_proteccion,codigo_bombero,usos.usos as "usos" FROM epis e LEFT JOIN usos USING(codigo)',
        ]);
        return $this ->render("datosEpis",[
            "resultados"=>$dataProvider,
            "campos"=>['codigo','nivel_proteccion','codigo_bombero','usos'],
            "titulo"=>"Epis",
            "descripcion"=>"Epis y sus usos",
            
        ]);
    }
    
    public function actionCuadrillas() {
        $dataProvider = new SqlDataProvider(['sql' =>'SELECT DISTINCT num_cuadrilla as "nº Cuadrilla",zona as "Parque",disponible,num_bomberos as "nº Bomberos" FROM cuadrillas e ',
        ]);
        return $this ->render("datosCuadrillas",[
            "resultados"=>$dataProvider,
//           
            "titulo"=>"Cuadrillas",
            "descripcion"=>"Cuadrillas de los parques",
            
        ]);
    }
    
    public function actionDtosbombero($datos){
//        $dataProvider = new SqlDataProvider(['sql' =>'SELECT codigo,idcuadrilla AS "Cuadrilla", rango, nombre, apellidos, bomberos.disponible AS "disponible",zona FROM bomberos INNER JOIN cuadrillas ON(idcuadrilla=cuadrillas.id) ',
//        ]);
//        $as[1]=array("j"=>"aish");
//        =["cod","sd"];
        
        $cosas = Yii::$app->db->createCommand("SELECT bomberos.codigo AS 'codigo' ,idcuadrilla AS 'Cuadrilla', rango, nombre, apellidos, bomberos.disponible AS 'disponible',zona ,fb.funciones AS 'funciones'
                                                FROM bomberos 
                                                INNER JOIN cuadrillas 
                                                ON(idcuadrilla=cuadrillas.id)
                                                LEFT JOIN funciones_bombero fb
                                                ON(bomberos.codigo=fb.codigo)
                                                where bomberos.codigo='$datos'")->queryAll();
        
         
        return Json::encode($cosas); 
    }

    public function actionFuncvehiculo($datos){
       $func = Yii::$app->db->createCommand("SELECT vehiculos.matricula AS 'Matricula', tipo, fv.funciones AS 'Funciones', zona
                                                FROM vehiculos 
                                                INNER JOIN funciones_vehiculo fv
                                                ON(fv.matricula=vehiculos.matricula)
                                                WHERE vehiculos.matricula='$datos'")->queryAll();

        if($func==true){
            return Json::encode($func);
        }else{
            return Json::encode("Vacio"); 
        }   

    }

    public function actionDtosalertas($datos){                
                $cosas = Yii::$app->db->createCommand("SELECT alertas.codigo as codigo,epis_utilizados,vehiculos_utilizados,tipo,descripcion,acuden.idcuadrilla as 'Numero Cuadrilla', zona 
                                                         FROM alertas 
                                                         LEFT JOIN acuden  USING(codigo) 
                                                        INNER JOIN cuadrillas ON(acuden.idcuadrilla=cuadrillas.id)
                                                        where alertas.codigo='$datos'")->queryAll();
                
                 
                return Json::encode($cosas); 
            }

     public function actionDtosparques($datos){                
            $cosas = Yii::$app->db->createCommand("SELECT id, zona, radio_accion
                                                        FROM parques 
                                                        where id='$datos'")->queryAll();
                
                 
                return Json::encode($cosas); 
            }

            public function actionDtoscuadrillas($datos){                
                $cosas = Yii::$app->db->createCommand("SELECT zona
                                                            FROM cuadrillas 
                                                            where num_cuadrilla='$datos'")->queryAll();
                    
                     
                    return Json::encode($cosas); 
                }

    public function actionModal(){

            $vista=$this->renderFile("@app/views/site/modalCrear.php");

            return $vista;
                 
                
            }

}


