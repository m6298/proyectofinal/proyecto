<?php

namespace app\controllers;

use app\models\Bomberos;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use Yii;
use yii\web\BadRequestHttpException;
use yii\web\Response;
use yii\widgets\ActiveForm;

/**
 * BomberosController implements the CRUD actions for Bomberos model.
 */
class BomberosController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Bomberos models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Bomberos::find(),
            /*
            'pagination' => [
                'pageSize' => 50
            ],
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC,
                ]
            ],
            */
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Bomberos model.
     * @param int $id ID
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {   
        
        return $this->render('view', [
            'model' => $this->findModel($id), 
        ]);
    }

    /**
     * Creates a new Bomberos model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new Bomberos();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model, 
        ]);
    }

    protected function isAjax() {

        $request = Yii::$app->request;

        if (!$request->isAjax) {

            throw new BadRequestHttpException('Bad Request Type!', '400');

        }

        return $request;

    }

    public function actionCreate2()
    {

        //$request = $this->isAjax();

        $model = new Bomberos();
/*
        if ($model->load($request->post())) {

            \Yii::$app->response->format = Response::FORMAT_JSON;

            if ($model->save()) {

                return ['message' => "Successfully added new {$model->name}.", 'id' => $model->id];

            }

            return ['errors' => ActiveForm::validate($model)];

        }*/ 
        
        if ($model->load(Yii::$app->request->post()) ) {
            
            if( $model->save()){
            return $this->redirect(['view', 'id' => $model->id]);
            }else{
                Yii::$app->session->setFlash('error', "Codigo repetido.");
                $dataProvider = new ActiveDataProvider([
                    'query' => Bomberos::find()
                       ->select("codigo,idcuadrilla AS Cuadrilla, rango, nombre, apellidos, bomberos.disponible AS disponible,zona" )
                       ->join("INNER JOIN","Cuadrillas", 'idcuadrilla=cuadrillas.id')
                       ,
                   'pagination' => [
                       'pageSize' => 1000,
                   ]
               ]);
                
        
                return $this ->render("..\site\datosBomberos2",[
                    "resultados"=>$dataProvider,
                    "campos"=>['codigo','Cuadrilla','rango','nombre','apellidos','disponible','zona'],
                    "titulo"=>"Bomberos",
                    "descripcion"=>"Bomberos que hay <p>Esta disponible si el valor es 1</p>",
                    
                ]);
            }
        } 
        elseif (Yii::$app->request->isAjax) {
            return $this->renderAjax('create2', [
                        'model' => $model
            ]);
        } 
        else {
            
            return $this->render('create2', [
                'model' => $model, 
            ]);
        }

        
    }

    /**
     * Updates an existing Bomberos model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $id ID
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Bomberos model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $id ID
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Bomberos model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $id ID
     * @return Bomberos the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Bomberos::findOne(['id' => $id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
