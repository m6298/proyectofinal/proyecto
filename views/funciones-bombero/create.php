<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\FuncionesBombero */

$this->title = 'Create Funciones Bombero';
$this->params['breadcrumbs'][] = ['label' => 'Funciones Bomberos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="funciones-bombero-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
