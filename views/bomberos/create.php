<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Bomberos */

$this->title = 'Create Bomberos';
$this->params['breadcrumbs'][] = ['label' => 'Bomberos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="bomberos-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
