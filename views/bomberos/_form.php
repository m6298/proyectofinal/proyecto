<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Bomberos */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="bomberos-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'codigo')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'idcuadrilla')->dropDownList($model->getdropdownIdCuadrillas(), ['prompt' => 'Seleccione Uno' ]) ?>
    
    <?= $form->field($model, 'rango')->dropDownList(["Oficial","Suboficial","Cabo"], ['prompt' => 'Seleccione Uno' ]) ?>

    <?= $form->field($model, 'nombre')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'apellidos')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'disponible')->checkbox() ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
