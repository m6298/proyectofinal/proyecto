<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\assets\AppAsset;
use app\widgets\Alert;
use yii\bootstrap4\Breadcrumbs;
use yii\bootstrap4\Html;
use yii\bootstrap4\Nav;
use yii\bootstrap4\NavBar;


AppAsset::register($this);
?>
<?php $this->beginPage() ?>

<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>" class="h-100">
<head >
    <link rel="shortcut icon" href="<?php echo Yii::$app->request->baseUrl; ?>/image/favicon.ico" type="image/x-icon" />
    
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body class="d-flex flex-column h-100">
<?php $this->beginBody() ?>

<header >
    <?php
    NavBar::begin([
        "brandLabel" => Html::img('@web/bomberos.png', ['style'=>'height:50px;'],['class'=>'navbar-brand clasetam'],['alt' => Yii::$app->name]),
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar navbar-expand-md navbar-dark fixed-top layfondo',
        ],
    ]);
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav'],
        'items' => [
            ['label' => 'Inicio', 'url' => ['/site/index']],
            [
              'label'=>'Gestión',
                'url'=>['ciclista/index'],
                'options'=>['class'=>'dropdown'],
                'template' => '<a href="{url}" class="href_class">{label}</a>',
                'items'=>[
                    ['label' => 'Acuden', 'url' => ['/acuden/index']],
                    ['label' => 'Alertas', 'url' => ['/alertas/index']],
                    ['label' => 'Bomberos', 'url' => ['/bomberos/index']],
                    ['label' => 'Cuadrillas', 'url' => ['/cuadrillas/index']],
                    ['label' => 'Epis', 'url' => ['/epis/index']],
                    ['label' => 'Funciones de bomberos', 'url' => ['/funciones-bombero/index']],
                    ['label' => 'Funciones de vehiculos', 'url' => ['/funciones-vehiculo/index']],
                    ['label' => 'Parques', 'url' => ['/parques/index']],
                    ['label' => 'Usos de epis', 'url' => ['/usos/index']],
                    ['label' => 'Vehiculos utilizados', 'url' => ['/utilizan/index']],
                    ['label' => 'Vehiculos', 'url' => ['/vehiculos/index']]
                ]
            ]
         ]
    ]);

    
    NavBar::end();
    ?>
</header>

<main role="main" class="flex-shrink-0">
    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</main>

<footer class="footer mt-auto py-3 text-muted">
    <div class="container">
        <p class="float-left">&copy; Mario Diaz Garcia <?= date('Y') ?></p>
        <p class="float-right">Aplicación Bomberos</p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
