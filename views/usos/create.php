<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Usos */

$this->title = 'Create Usos';
$this->params['breadcrumbs'][] = ['label' => 'Usos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="usos-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
