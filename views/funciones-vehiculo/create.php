<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\FuncionesVehiculo */

$this->title = 'Create Funciones Vehiculo';
$this->params['breadcrumbs'][] = ['label' => 'Funciones Vehiculos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="funciones-vehiculo-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
