<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Acuden */



?>
<div class="acuden-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model, 'codigo'=>$codigo,
    ]) ?>

</div>
