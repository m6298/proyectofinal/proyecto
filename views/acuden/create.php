<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Acuden */

$this->title = 'Create Acuden';
$this->params['breadcrumbs'][] = ['label' => 'Acudens', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="acuden-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
