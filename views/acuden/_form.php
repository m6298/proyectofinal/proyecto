<?php

use yii\helpers\Html;
use yii\helpers\VarDumper;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Acuden */
/* @var $form yii\widgets\ActiveForm */


?>



<div class="acuden-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'codigo')->textInput(['value'=>$codigo, 'readonly'=>true]) ?>

    <?= $form->field($model, 'idcuadrilla')->dropDownList($model->getdropdownCuadrillas(), ['prompt' => 'Seleccione Uno' ]) ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
