<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Cuadrillas */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cuadrillas-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'num_cuadrilla')->textInput() ?>

    

    <?= $form->field($model, 'disponible')->checkbox() ?>

    <?= $form->field($model, 'num_bomberos')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
