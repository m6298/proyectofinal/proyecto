<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Utilizan */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="utilizan-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'codigo_bombero')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'matricula')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
