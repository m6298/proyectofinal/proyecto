<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Epis */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="epis-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nivel_proteccion')->dropDownList($model->getdropdownNivelProt(), ['prompt' => 'Seleccione Uno' ]) ?>

    <?= $form->field($model, 'codigo_bombero')->dropDownList($model->getdropdownCodigoBombero(), ['prompt' => 'Seleccione Uno' ])?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
