<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Epis */

$this->title = 'Update Epis: ' . $model->codigo;
$this->params['breadcrumbs'][] = ['label' => 'Epis', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->codigo, 'url' => ['view', 'codigo' => $model->codigo]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="epis-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
