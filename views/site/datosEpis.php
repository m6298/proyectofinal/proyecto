<?php
use yii\grid\GridView;
use yii\helpers\Html;
use yii\bootstrap4\Modal;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use kartik\icons\Icon;
use yii\widgets\ListView;

Icon::map($this, Icon::FA);

?>

<script src="<?php Yii::$app->request->baseUrl?>\js\javaScript.js"></script>

<div class="jumbotron">
    <h2><?=$titulo?></h2>                        
    
</div>


<!-- Boton para acceder a la funcion de ajax que abre el modal de añadir vehiculos -->
<?= Html::button('Nuevo Epi', 
        ['value'=>Url::to(['epis/create2']),
                        'class' => 'btn btn-success ','id'=>'modalButton']) ?>

<div class="row">
    <?=Html::a(Icon::show('table', ['class' => 'fa-solid', 'framework' => Icon::FAS]),['/site/epis'], ['class' => 'btn btn-secondary separacion seleccionado'])?>
    
</div>

<?php
  Modal::begin([
     'title' =>'<h4>Ficha</h4>',
     'id'     =>'modal',
     'size'   =>'modal-lg',
     'clientOptions' => ['backdrop' => 'static', 'keyboard' => FALSE]
     ]);
  echo "<div id='modalContent'> </div>";
  Modal::end();
?>


<?=GridView::widget([
    'dataProvider' => $resultados,
    'columns'=>['codigo',
    [
        'attribute'=>'codigo_bombero',
        'value'=>function($data){
            if($data['codigo_bombero']==null){
                $disp="Repuesto";
            }
            else{
                $disp=$data['codigo_bombero'];
            }
            return $disp;
        }
    ],
    'nivel_proteccion',
    [
        'attribute'=>'usos',
        'value'=>function($data){
            if($data['usos']==null){
                $disp="Generico";
            }
            else{
                $disp=$data['usos'];
            }
            return $disp;
        }
    ],
    
        
        
         ],
]); 
?>
