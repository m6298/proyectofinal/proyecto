<?php
use yii\helpers\Html;
/* @var $this yii\web\View */



$this->title = 'My Yii Application';
?>
<div class="site-index separriba">

    <div class="body-content">

        <div class="row">
            <div class="col-sm-6 col-md-4" >
                <div class="card alturaminima">
                    <div class="card-body tarjeta">
                        <h3>Bomberos</h3>
                        
                        
                        <img src="<?php echo Yii::$app->request->baseUrl; ?>/image/bombero1.png "  height="200px">
                        <?=Html::a('Bomberos',['/site/bomberos'], ['class' => 'btn btn-secondary sepboton'])?>
                        
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-md-4">
                <div class="card alturaminima">
                    <div class="card-body tarjeta">
                        <h3>Vehículos</h3>
                        <img src="<?php echo Yii::$app->request->baseUrl; ?>/image/vehiculo1.png "  height="170px">
                        <?=Html::a('Vehiculos',['/site/vehiculos'], ['class' => 'btn btn-secondary sepboton'])?>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-md-4">
                <div class="card alturaminima">
                    <div class="card-body tarjeta">
                        <h3>Alertas</h3>
                        <img src="<?php echo Yii::$app->request->baseUrl; ?>/image/alerta.png "  height="170px">
                         <?=Html::a('Alertas',['/site/alertas'], ['class' => 'btn btn-secondary sepboton'])?>
                        
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6 col-md-4" >
                <div class="card alturaminima">
                    <div class="card-body tarjeta">
                        <h3>Parques</h3>
                        <img src="<?php echo Yii::$app->request->baseUrl; ?>/image/estacion.png "  height="170px">
                        <?=Html::a('Parques',['/site/parques'], ['class' => 'btn btn-secondary sepboton'])?>                        
                        
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-md-4" >
                <div class="card alturaminima">
                    <div class="card-body tarjeta">
                        <h3>Epis</h3>
                        <img src="<?php echo Yii::$app->request->baseUrl; ?>/image/epis1.png "  height="190px">
                        <?=Html::a('Epis',['/site/epis'], ['class' => 'btn btn-secondary sepboton'])?>     
                        
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-md-4" >
                <div class="card alturaminima">
                    <div class="card-body tarjeta">
                        <h3>Cuadrillas</h3>
                        <img src="<?php echo Yii::$app->request->baseUrl; ?>/image/cuadrilla2.png "  height="170px">
                        <?=Html::a('Cuadrillas',['/site/cuadrillas'], ['class' => 'btn btn-secondary sepboton'])?>     
                        
                    </div>
                </div>
            </div>




            </div>
        </div>

    </div>
</div>
