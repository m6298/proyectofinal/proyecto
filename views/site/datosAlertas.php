<?php
use yii\grid\GridView;
use yii\helpers\Html;
use yii\bootstrap4\Modal;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use kartik\icons\Icon;
use yii\widgets\ListView;

Icon::map($this, Icon::FA);

?>

<script src="<?php Yii::$app->request->baseUrl?>\js\javaScript.js"></script>

<div class="jumbotron">
    <h2><?=$titulo?></h2>                        
    
</div>


<!-- Boton para acceder a la funcion de ajax que abre el modal de añadir vehiculos -->
<?= Html::button('Nueva Alerta', 
        ['value'=>Url::to(['alertas/create2']),
                        'class' => 'btn btn-success ','id'=>'modalButton']) ?>

<div class="row">
    <?=Html::a(Icon::show('table', ['class' => 'fa-solid', 'framework' => Icon::FAS]),['/site/alertas'], ['class' => 'btn btn-secondary separacion seleccionado'])?>
    <?=Html::a(Icon::show('address-book', ['class' => 'fa-solid', 'framework' => Icon::FAS]),['/site/alertas2'], ['class' => 'btn btn-secondary separacion2'])?>
</div>

<?php
  Modal::begin([
     'title' =>'<h4>Ficha</h4>',
     'id'     =>'modal',
     'size'   =>'modal-lg',
     'clientOptions' => ['backdrop' => 'static', 'keyboard' => FALSE]
     ]);
  echo "<div id='modalContent'> </div>";
  Modal::end();
?>


<?=GridView::widget([
    'dataProvider' => $resultados,
    'columns'=>['codigo','Numero Cuadrilla','tipo','zona',
        
        ['class' => 'yii\grid\ActionColumn',
            'header' => 'Botones',
            'template' => '{boton}',
            'buttons' => [
                'boton' => function($url, $model) {
    
    
            /*
                Boton para cargar el modal utilizando ajax se utiliza la logica para mostrar si esta disponible o no, para mostrar funciones en caso de que tenga el bombero
                y para cargar los mapas respectivos de cada zona
            */
                return  Html::tag('p', 'Mas Info', ['class' => 'btn btn-secondary','type'=>'button',
                    'onclick' => '
                                        $.ajax({
                                            url: "'.Url::toRoute(['/site/dtosalertas?datos='.$model['codigo']]).'",
                                            type: "post",
                                            contentType: "application/json",
                                            dataType:"json",
                                            success: function(data){
                                                
                                                iniciarMap();
                                                $("#codigo").val(data[0]["codigo"]);
                                                $("#epis").val(data[0]["epis_utilizados"]);
                                                $("#vehiculos").val(data[0]["vehiculos_utilizados"]);
                                              
                                                $("#cuadrilla").val(data[0]["Numero Cuadrilla"]);
                                                $("#tipo").val(data[0]["tipo"]);
                                                $("#descripcion").val(data[0]["descripcion"]);
                                                
                                                $("#zona").val(data[0]["zona"]);

                                                document.getElementById("map").style.display = "none";
                                                document.getElementById("map2").style.display = "none";
                                                document.getElementById("map3").style.display = "none";
                                                document.getElementById("map4").style.display = "none";
                                                document.getElementById("map5").style.display = "none";
                                                
                                                if(data[0]["zona"]=="Santander"){
                                                    document.getElementById("map").style.display = "block";
                                                }else if(data[0]["zona"]=="Torrelavega"){
                                                    document.getElementById("map2").style.display = "block";
                                                }else if(data[0]["zona"]=="Colindres"){
                                                    document.getElementById("map3").style.display = "block";
                                                }else if(data[0]["zona"]=="Piélagos"){
                                                    document.getElementById("map4").style.display = "block";
                                                }else if(data[0]["zona"]=="Valdáliga"){
                                                    document.getElementById("map5").style.display = "block";
                                                }
                                                
                                                $("#modalp").modal("show");
                                            
                                            },
                                            error: function () {
                                                alert("ERROR");
                                            }
                                        });
                                    '
                     ]);
                },  
             ],
            ],
         ],
]); 
?>

<h2 class="espacio">Alertas sin asignar</h2>

<?=GridView::widget([
    'dataProvider' => $sincodigo,
    'columns'=>['codigo' , 'epis_utilizados', 'vehiculos_utilizados','tipo',
        
        ['class' => 'yii\grid\ActionColumn',
            'header' => 'Botones',
            'template' => '{boton}',
            'buttons' => [
                'boton' => function($url, $model) {
    
    
            /*
                Boton para cargar el modal utilizando ajax se utiliza la logica para mostrar si esta disponible o no, para mostrar funciones en caso de que tenga el bombero
                y para cargar los mapas respectivos de cada zona
            */
                return   Html::tag('p','Asignar alerta', 
                            ['value' => Url::to(['acuden/create2','datos'=>$model['codigo']]),
                                'type'=>'button','class' => 'btn btn-success modalButton2','id'=>'modalButton2']) ;

                                 
                },  
             ],
            ],
         ],
]); 
?>



<div class="modal" tabindex="-1" role="dialog" id="modalp">
  <div class="modal-dialog modal-lg " role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Alerta</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
            <h4>Código</h4>
          <input readonly="readonly" class="form-control-lg " id="codigo"></input>
          <h4>Epis utilizados</h4>
          <input readonly="readonly" class="form-control-lg " id="epis"></input>
          <div>
          <h4>Vehiculos utilizados</h4>
          <input readonly="readonly" class="form-control-lg " id="vehiculos"></input>
          <h4>Cuadrilla</h4>
          <input readonly="readonly" class="form-control-lg " id="cuadrilla"></input>
          <h4>Tipo</h4>
          <input readonly="readonly" class="form-control-lg " id="tipo"></input>
          <h4>Descripción</h4>
          <input readonly="readonly" class="form-control-lg " id="descripcion"></input>
          <h4>Zona</h4>
          <input readonly="readonly" class="form-control-lg " id="zona"></input>
          <?php 
     //     Html::tag('p', 'hola', ['class' => 'btn btn-secondary','type'=>'button',
     //               'onclick' => 'https://maps.googleapis.com/maps/api/js?key=AIzaSyBDaeWicvigtP9xPv919E-RNoxfvC-Hqik&callback=iniciarMap']);
      //              Html::a('Profile', ['https://maps.googleapis.com/maps/api/js?key=AIzaSyBDaeWicvigtP9xPv919E-RNoxfvC-Hqik&callback=iniciarMap', 'id' => "prub"], ['class' => 'profile-link']) ;
                    ?>
                                        
          <div id="map" class="map"></div>
          <div id="map2" class="map"></div>
          <div id="map3" class="map"></div>
          <div id="map4" class="map"></div>
          <div id="map5" class="map"></div>
          <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBDaeWicvigtP9xPv919E-RNoxfvC-Hqik&callback=iniciarMap"></script>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>