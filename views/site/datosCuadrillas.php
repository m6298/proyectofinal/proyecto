<?php
use yii\grid\GridView;
use yii\helpers\Html;
use yii\bootstrap4\Modal;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use kartik\icons\Icon;
use yii\widgets\ListView;

Icon::map($this, Icon::FA);

?>

<script src="<?php Yii::$app->request->baseUrl?>\js\javaScript.js"></script>




<div class="jumbotron" >
    <h2><?=$titulo?></h2>
    
    
    
</div>

<!-- Boton para acceder a la funcion de ajax que abre el modal de añadir bomberos -->
<?= Html::button('Nueva Cuadrilla', 
        ['value'=>Url::to(['cuadrillas/create2']),
                        'class' => 'btn btn-success ','id'=>'modalButton']) ?>


<div class="row">
    <?=Html::a(Icon::show('table', ['class' => 'fa-solid', 'framework' => Icon::FAS]),['/site/cuadrillas'], ['class' => 'btn btn-secondary separacion seleccionado'])?>
    
</div>


<?php
  Modal::begin([
     'title' =>'<h4>Ficha</h4>',
     'id'     =>'modal',
     'size'   =>'modal-lg',
     'clientOptions' => ['backdrop' => 'static', 'keyboard' => FALSE]
     ]);
  echo "<div id='modalContent'> </div>";
  Modal::end();
?>

<?=GridView::widget([
    'dataProvider' => $resultados,
    'columns'=>["nº Cuadrilla", "Parque","nº Bomberos",
    [
        'attribute'=>'disponible',
        'value'=>function($data){
            if(intval($data['disponible'])==1){
                $disp="si";
            }
            else{
                $disp="no";
            }
            return $disp;
        }
    ],
        
        ['class' => 'yii\grid\ActionColumn',
            'header' => 'Botones',
            'template' => '{boton}',
            'buttons' => [
                'boton' => function($url, $model) {
    
    
            /*
                Boton para cargar el modal utilizando ajax se utiliza la logica para mostrar si esta disponible o no, para mostrar funciones en caso de que tenga el bombero
                y para cargar los mapas respectivos de cada zona
            */
                return  Html::tag('p', 'Ver Mapa', ['class' => 'btn btn-secondary','type'=>'button',
                    'onclick' => '
                                        $.ajax({
                                            url: "'.Url::toRoute(['/site/dtoscuadrillas?datos='.$model['nº Cuadrilla']]).'",
                                            type: "post",
                                            contentType: "application/json",
                                            dataType:"json",
                                            success: function(data){
                                                iniciarMap();

                                                document.getElementById("map").style.display = "none";
                                                document.getElementById("map2").style.display = "none";
                                                document.getElementById("map3").style.display = "none";
                                                document.getElementById("map4").style.display = "none";
                                                document.getElementById("map5").style.display = "none";
                                                
                                                if(data[0]["zona"]=="Santander"){
                                                    document.getElementById("map").style.display = "block";
                                                }else if(data[0]["zona"]=="Torrelavega"){
                                                    document.getElementById("map2").style.display = "block";
                                                }else if(data[0]["zona"]=="Colindres"){
                                                    document.getElementById("map3").style.display = "block";
                                                }else if(data[0]["zona"]=="Piélagos"){
                                                    document.getElementById("map4").style.display = "block";
                                                }else if(data[0]["zona"]=="Valdáliga"){
                                                    document.getElementById("map5").style.display = "block";
                                                }
                                                
                                                $("#modalp").modal("show");
                                                
                                            },
                                            error: function () {
                                                alert("ERROR");
                                            }
                                        });
                                    '
                     ]);
                
                },  
             ],
            ],
         ],
]); 
?>



<?php
 
?>

<div id="crear_bombero"></div>

<div class="modal" tabindex="-1" role="dialog" id="modalt">
  <div class="modal-dialog modal-lg " role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Nuevo Bombero</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
        <div class="modal-body" id="cuerpo">


        </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

     <!--codigo del modal para cargar utilizando ajax-->
     
 <div class="modal" tabindex="-1" role="dialog" id="modalp">
  <div class="modal-dialog modal-lg " role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Cuadrillas</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
                                        
          <div id="map" class="map"></div>
          <div id="map2" class="map"></div>
          <div id="map3" class="map"></div>
          <div id="map4" class="map"></div>
          <div id="map5" class="map"></div>
          <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBDaeWicvigtP9xPv919E-RNoxfvC-Hqik&callback=iniciarMap"></script>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
     