<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Alertas */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="alertas-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'epis_utilizados')->textInput() ?>

    <?= $form->field($model, 'vehiculos_utilizados')->textInput() ?>

    <?= $form->field($model, 'tipo')->dropDownList($model->getdropdownTipos(), ['prompt' => 'Seleccione Uno' ])?>

    <?= $form->field($model, 'descripcion')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
